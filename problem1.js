
function problem1(inventory,cid){
    if(inventory==undefined||inventory.length==0||cid==undefined){
        return []
    }
    for(let i=0;i<inventory.length;i++){
        if(inventory[i]==undefined){
            continue;
        }
        if(inventory[i].id==cid){
            return inventory[i];
        }
    }
    return []
}

module.exports=problem1
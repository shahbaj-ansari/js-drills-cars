

function problem4(inventory){

    if(inventory==undefined||inventory.length==0){
        return []
    }

    const problem3=require('./problem3')
    const carByYear=problem3(inventory)
    return carByYear
}

module.exports=problem4
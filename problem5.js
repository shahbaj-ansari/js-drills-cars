
function problem5(inventory){

    if(inventory==undefined||inventory.length==0){
        return []
    }

    const problem4 = require("./problem4");
    const carByYear=problem4(inventory)

    let oldercar=[]
    for(let i=0;i<carByYear.length;i++){
        let count=0
        for(let j=0;j<carByYear[i].years.length;j++){
            if(carByYear[i].years[j]<2000){
                count++;
            }
        }
        if(count){
            oldercar.push(carByYear[i].carMake)
        }
    }
    return oldercar;
}


module.exports=problem5
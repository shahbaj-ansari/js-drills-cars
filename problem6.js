
function problem6(inventory){

    if(inventory==undefined||inventory.length==0){
        return []
    }

    let BMWAndAudiData=[]
    for(let i=0;i<inventory.length;i++){
        if(inventory[i]==undefined){
            continue;
        }
    if(inventory[i].car_make=='BMW'||inventory[i].car_make=='Audi'){
        BMWAndAudiData.push(inventory[i])
        }
    }
    return BMWAndAudiData
}

module.exports=problem6
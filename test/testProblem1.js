
const inventory = require('../inventory.js');
const problem1= require('../problem1.js');

const result=problem1(inventory,33)

if(result.length==0){
    console.log('wrong credentials passed.')
}else{
    console.log(`Car ${result.id} is a ${result.car_year} ${result.car_make} ${result.car_model}`)
}

const inventory = require('../inventory');
const problem2= require('../problem2');

const result=problem2(inventory)

if(result.length==0){
    console.log('Inventory is Empty.')
}else{
    console.log(`Last car is a ${result.car_year} ${result.car_make} ${result.car_model}`)
}

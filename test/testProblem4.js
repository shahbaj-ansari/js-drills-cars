const inventory = require('../inventory');
const problem4= require('../problem4');

const result=problem4(inventory)

if(result.length==0){
    console.log('Inventory is Empty.')
}else{
    for(let i=0;i<result.length;i++){
        console.log(`${result[i].carMake} -> ${result[i].years}`);
    }
}